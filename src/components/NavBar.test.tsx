import React from 'react';
import { shallow } from 'enzyme';

import NavBar from './NavBar';

it('should render correctly with default props', () => {
  const component = shallow(<NavBar />);
  expect(component).toMatchSnapshot();
});
