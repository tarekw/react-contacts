import React from 'react';
import TestRenderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { MockedProvider } from '@apollo/react-testing';
import { BrowserRouter as Router } from 'react-router-dom';
import wait from 'waait';

import {
  ContactForm,
  basicEmailCheck,
  isContactValid,
} from './ContactForm';

import {
  GET_CONTACT,
  UPDATE_CONTACT,
  ALL_CONTACTS,
} from './constants';

/**
 * Suppress React 16.8 act() warnings globally.
 * The react teams fix won't be out of alpha until 16.9.0.
 */
const consoleError = console.error;
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation((...args) => {
    if (!args[0].includes('Warning: An update to %s inside a test was not wrapped in act')) {
      consoleError(...args);
    }
  });
});

const mocks = [
  {
    request: {
      query: ALL_CONTACTS,
    },
    result: {
      data: {
        contacts: [
          {
            name: "John Doe",
            id: "1"
          },
          {
            name: "Janet Doe",
            id: "2"
          }
        ]
      }
    },
  },
  {
    request: {
      query: GET_CONTACT,
      variables: {
        id: '1',
      },
    },
    result: {
      data: {
        contact: {
          id: '1',
          name: 'John Doe',
          email: 'john@doe.com',
          modified: '06-10-2019 22:38',
          created: '05-10-2019 14:56'
        }
      },
    },
  },
  {
    request: {
      query: GET_CONTACT,
      variables: {
        id: '1',
      },
    },
    result: {
      data: {
        contact: {
          id: '1',
          name: 'John Doe',
          email: 'john@doe.com',
          modified: '06-10-2019 22:38',
          created: '05-10-2019 14:56'
        }
      },
    },
  },
  {
    request: {
      query: UPDATE_CONTACT,
      variables: {
        contact: {
          id: '1',
          name: "John Doe",
          email: 'john@doe.com',
        }
      },
    },
    result: {
      data: {
        contact: {
          id: '1',
        }
      },
    },
  },
];

it('should render correctly with default props', () => {
  const component = shallow(<ContactForm />);
  expect(component).toMatchSnapshot();
});

it('should render correctly no id present', () => {
  const match = {
      params: {},
  };
  const props = {
      match,
  }

  const component = shallow(<ContactForm {...props} />);
  expect(component).toMatchSnapshot();
});

it('should render correctly when id present', () => {
  const match = {
      params: {
        id: 1,
      },
  };
  const props = {
      match,
  }

  const component = shallow(<ContactForm {...props} />);
  expect(component).toMatchSnapshot();
});

it('should render the form with prefilled values if id passed in', async () => {
  const match = {
    params: {
        id: '1',
    },
  };
  const props = {
      match,
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactForm {...props} />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  expect(component).toMatchSnapshot();
});

it('should show all contacts when contact saved', async () => {
  const pushMock = jest.fn();
  const match = {
    params: {
        id: '1',
    },
  };
  const props = {
    match,
    history: {
      push: pushMock,
    },
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactForm {...props} />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  const firstItem = component.root.findAllByType('button')[0];

  firstItem.props.onClick();

  await wait(0);
  await wait(0);

  expect(pushMock).toHaveBeenCalledTimes(1);
  expect(pushMock).toHaveBeenCalledWith('/contacts');
});

it.skip('should handle change', () => {
  // TODO
})

describe('util functions', () => {
  it('should check basic email format', () => {
    expect(basicEmailCheck('a@bc')).toBe(false);
    expect(basicEmailCheck('ab.c')).toBe(false);
    expect(basicEmailCheck('a@b.c')).toBe(true);
  });

  it('should check contact is valid', () => {
    expect(isContactValid({ name: '', email: 'a@b.c' })).toBe(false);
    expect(isContactValid({ name: 'name', email: 'ab.c' })).toBe(false);
    expect(isContactValid({ name: 'name', email: 'a@b.c' })).toBe(true);
  });
})
