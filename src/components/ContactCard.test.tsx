import React from 'react';
import TestRenderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { MockedProvider } from '@apollo/react-testing';
import { BrowserRouter as Router } from 'react-router-dom';
import wait from 'waait';

import ContactCard from './ContactCard';
import { GET_CONTACT } from './constants';

/**
 * Suppress React 16.8 act() warnings globally.
 * The react teams fix won't be out of alpha until 16.9.0.
 */
const consoleError = console.error;
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation((...args) => {
    if (!args[0].includes('Warning: An update to %s inside a test was not wrapped in act')) {
      consoleError(...args);
    }
  });
});

const mocks = [
  {
    request: {
      query: GET_CONTACT,
      variables: {
        id: '1',
      },
    },
    result: {
      data: {
        contact: {
          id: '1',
          name: 'John Doe',
          email: 'john@doe.com',
          modified: '06-10-2019 22:38',
          created: '05-10-2019 14:56'
        }
      },
    },
  },
];

it('should render correctly with default props', () => {
  const component = shallow(<ContactCard />);
  expect(component).toMatchSnapshot();
});

it('should render correctly when id passed', () => {
  const match = {
      params: {
          id: '1',
      },
  };
  const props = {
      match,
  };
  const component = shallow(<ContactCard  {...props}/>);
  expect(component).toMatchSnapshot();
});

it('should render loading state initially', () => {
  const match = {
    params: {
        id: '1',
    },
  };
  const props = {
      match,
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={[]} addTypename={false}>
      <Router>
        <ContactCard {...props} />
      </Router>
    </MockedProvider>
  );

  expect(component).toMatchSnapshot();
});

it('should render the card with mocked values', async () => {
  const match = {
    params: {
        id: '1',
    },
  };
  const props = {
      match,
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactCard {...props} />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  expect(component).toMatchSnapshot();
});
