import React from 'react';
import TestRenderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { MockedProvider } from '@apollo/react-testing';
import { BrowserRouter as Router } from 'react-router-dom';
import wait from 'waait';

import { ContactsList } from './ContactsList';
import {
  ALL_CONTACTS,
} from './constants';

/**
 * Suppress React 16.8 act() warnings globally.
 * The react teams fix won't be out of alpha until 16.9.0.
 */
const consoleError = console.error;
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation((...args) => {
    if (!args[0].includes('Warning: An update to %s inside a test was not wrapped in act')) {
      consoleError(...args);
    }
  });
});

const mocks = [
  {
    request: {
      query: ALL_CONTACTS,
    },
    result: {
      data: {
        contacts: [
          {
            name: "John Doe",
            id: "1"
          },
          {
            name: "Janet Doe",
            id: "2"
          }
        ]
      }
    },
  },
];
  
it('should render correctly with default props', () => {
  const component = shallow(<ContactsList />);
  expect(component).toMatchSnapshot();
});

it('should handle item click', async () => {
  const pushMock = jest.fn();
  const props = {
    history: {
      push: pushMock,
    },
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactsList {...props} />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  const firstItem = component.root.findAllByType('div')[0];

  firstItem.props.onClick();

  expect(pushMock).toHaveBeenCalledTimes(1);
  expect(pushMock).toHaveBeenCalledWith('/contact/1');
});

it('should handle button click', async () => {
  const pushMock = jest.fn();
  const props = {
    history: {
      push: pushMock,
    },
  };

  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactsList {...props} />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  const firstButton = component.root.findAllByType('button')[0];

  firstButton.props.onClick();

  expect(pushMock).toHaveBeenCalledTimes(1);
  expect(pushMock).toHaveBeenCalledWith('/update/1');
});

it('should render the contacts list with mocked values', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router>
        <ContactsList />
      </Router>
    </MockedProvider>
  );

  await wait(0);

  expect(component).toMatchSnapshot();
});

it.skip('should handle delete', () => {
  // TODO
});