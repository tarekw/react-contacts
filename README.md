# Contact Manager

## Features implemented as per spec

- List Contacts :heavy_check_mark:
- View Contact :heavy_check_mark:
- Add Contact :heavy_check_mark:
- Delete Contact :heavy_check_mark:
- Edit Contact :heavy_check_mark:
- Clean routing i.e '/contact/:id' :heavy_check_mark:
- Use material-ui for components :heavy_check_mark:

### Bonuses

- Use [xstate](https://xstate.js.org/docs) to manage app state. - Ongoing
- Connect to the graphql endpoint http://localhost:3001 by using create-react-app proxy feature :heavy_check_mark:
- Use the graphql endpoint to get/create/update/delete :heavy_check_mark:

### Additional Work (Ongoing)

- Improve typings
- Additional unit tests :heavy_check_mark:
- Snapshot tests :heavy_check_mark:
- Integration tests (e.g. testcafe/wdio)

### Contact

- Name eg 'John Smith'
- Email eg 'john@smith.com'
- Date Modified eg '31-01-2018 15:04'
- Date Created eg '31-01-2018 15:04'

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run gql`

Runs the graphql server.

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
